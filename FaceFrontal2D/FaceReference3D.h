#ifndef __FACE_REFERENCE__
#define __FACE_REFERENCE__

#include <iostream>
#include <string>
#include <fstream>
#include <vector>

#include "opencv2\core\core.hpp"

namespace face_frontal {
	class FaceReference3D {
	public:
		FaceReference3D();
		FaceReference3D(std::string name) : _fModelName(name) {
			Read3DModel();
		};
	//protected:
		cv::Mat_<float> refU;
		cv::Mat_<float> outA;
		cv::Mat_<float> ref_XY;
		cv::Mat_<float> p3d;

		void Read3DModel();
	private:
		std::string _fModelName;
	};
}

#endif