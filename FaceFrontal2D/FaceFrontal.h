#ifndef __FACE_FRONTAL__
#define __FACE_FRONTAL__

#define DLIB_JPEG_SUPPORT
#define DLIB_PNG_SUPPORT

#include "dlib\image_processing\frontal_face_detector.h"
#include "dlib\image_processing\render_face_detections.h"
#include "dlib\image_processing.h"
#include "dlib\gui_widgets.h"
#include "dlib\image_io.h"

#include <iostream>
#include <string>
#include <algorithm>

#include "opencv2\core\core.hpp"
#include "opencv2\calib3d\calib3d.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\highgui\highgui.hpp"

#include "FaceReference3D.h"

namespace face_frontal {
	static const int WIDTH = 250; 
	static const int HEIGHT = 250;
	static const int ACC_CONST = 800;

	struct Point {
	public:
		Point() {}
		Point(int _x, int _y) : x(_x), y(_y) {}
		int x;
		int y;
	};

	struct FaceRectangle {
	public:
		FaceRectangle() {}
		FaceRectangle(Point _tl, Point _br) : tl(_tl), br(_br) {}

		Point tl; //top left (left, too)
		Point br;  //bottom right (right, bottom)

		int left() { return tl.x; }
		int top() { return tl.y; }
		int width() { return br.x - tl.x; }
		int height() { return br.y - tl.y; }
	};

	class FaceFrontal2D : face_frontal::FaceReference3D {
	public:
		FaceFrontal2D();
		FaceFrontal2D(std::string model_name, std::string img_file_name, std::string ref_model_name);
		cv::Mat frontalization();
	private:
		void FaceFrontalFeature2D();
		void rescale();
		cv::Mat GetProjectionMat();
		void bilinearInterp(cv::Mat&, cv::Mat_<double>&, std::vector<int>&, cv::Size, cv::Mat&);
		
		std::string _mName;
		std::string _fName;

		cv::Mat _originImg;
		std::vector<cv::Point2f> _feature2D;
		face_frontal::FaceRectangle _featureRect;
		cv::Mat _rescaleImg;
	};
}

#endif