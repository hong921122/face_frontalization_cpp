#include "opencv2\core\core.hpp"
#include "opencv2\core\mat.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\highgui\highgui.hpp"

#include <iostream>
#include <vector>

#include "FaceReference3D.h"
#include "FaceFrontal.h"

using namespace cv;
Mat matlab_reshape(const Mat &m, int new_row, int new_col, int new_ch)
{
	int old_row, old_col, old_ch;
	old_row = m.size().height;
	old_col = m.size().width;
	old_ch = m.channels();

	Mat m1(1, new_row*new_col*new_ch, m.depth());

	std::vector <Mat> p(old_ch);
	split(m, p);
	for (int i = 0; i<p.size(); ++i){
		Mat t(p[i].size().height, p[i].size().width, m1.type());
		t = p[i].t();
		Mat aux = m1.colRange(i*old_row*old_col, (i + 1)*old_row*old_col).rowRange(0, 1);
		t.reshape(0, 1).copyTo(aux);
	}

	std::vector <Mat> r(new_ch);
	for (int i = 0; i<r.size(); ++i){
		Mat aux = m1.colRange(i*new_row*new_col, (i + 1)*new_row*new_col).rowRange(0, 1);
		r[i] = aux.reshape(0, new_col);
		r[i] = r[i].t();
	}

	Mat result;
	merge(r, result);
	return result;
}

int main() {
	face_frontal::FaceFrontal2D test = face_frontal::FaceFrontal2D("shape_predictor_68_face_landmarks.dat", "test.jpg", "document.yml");
	test.frontalization();

	//std::vector< std::vector<cv::Point3f> > vect;
	//std::vector<cv::Point3f> vec;
	//vec.push_back(cv::Point3f(1.0f, 2.0f, 333.032f));
	//vec.push_back(cv::Point3f(1.0f, 2.0f, 333.032f));
	//vec.push_back(cv::Point3f(1.0f, 2.0f, 333.032f));
	//vec.push_back(cv::Point3f(1.0f, 2.0f, 333.032f));
	//vec.push_back(cv::Point3f(1.0f, 2.0f, 333.032f));
	//vec.push_back(cv::Point3f(1.0f, 2.0f, 333.032f));
	//vec.push_back(cv::Point3f(1.0f, 2.0f, 333.032f));
	//vec.push_back(cv::Point3f(1.0f, 2.0f, 333.032f));
	//vec.push_back(cv::Point3f(1.0f, 2.0f, 333.032f));
	//cv::Mat aa = cv::Mat(vect, CV_32FC2);
	//std::cout << aa << std::endl;
	//std::cout << aa.reshape(aa.channels(), 1) << std::endl;
	
	//int size[] = { 2, 2 };
	//std::cout << aa.reshape(3, 1, size) << std::endl;
	//int size[] = { 100, 100, 100 };
	//int zz[] = { 3, 3 };

	//int sizes[] = { 3, 3, 3 };

	//cv::Mat matrix = cv::Mat(3, sizes, CV_32FC1, cv::Scalar(0));
	//std::cout << matrix << std::endl;
	
	
	//
	//cv::Mat* b = new cv::Mat(2, zz, CV_32FC1, cv::Scalar(0));
	//std::cout << b->size() << std::endl;
	//
	//std::cout << b->reshape(1).t().size() << std::endl;
	//
	//std::cout << *b << std::endl;


	//cv::Mat out = (cv::Mat_<double>(3, 3) << 492.3077, 0, 160, 0, 492.3077, 160, 0, 0, 1);
	//std::cout << out << std::endl;
	//cv::Mat distCoeffs = cv::Mat::zeros(5, 1, CV_32F);
	//std::cout << distCoeffs << std::endl;
}