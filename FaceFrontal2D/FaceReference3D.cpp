#include "FaceReference3D.h"

face_frontal::FaceReference3D::FaceReference3D() {
	this->outA = (cv::Mat_<double>(3, 3) << 492.3077, 0, 160, 0, 492.3077, 160, 0, 0, 1);
}

void face_frontal::FaceReference3D::Read3DModel() {
	cv::FileStorage fs(this->_fModelName, cv::FileStorage::READ);

	if (!fs.isOpened()) {
		return;
	}

	fs["refXY"] >> this->ref_XY;
	fs["threedee"] >> this->p3d;
	fs["refU"] >> this->refU;
	fs["outA"] >> this->outA;

	fs.release();
}