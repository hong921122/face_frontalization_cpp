#include "FaceFrontal.h"

face_frontal::FaceFrontal2D::FaceFrontal2D() {}

face_frontal::FaceFrontal2D::FaceFrontal2D(std::string model_name, std::string file_name, std::string ref_model_name) :
_mName(model_name), _fName(file_name), FaceReference3D(ref_model_name)
{
	cv::Mat src = cv::imread(file_name);
	cv::pyrUp(src, this->_originImg, cv::Size(src.cols * 2, src.rows * 2));
}

void face_frontal::FaceFrontal2D::FaceFrontalFeature2D() {
	try {
		dlib::frontal_face_detector detector = dlib::get_frontal_face_detector();

		dlib::shape_predictor sp;
		dlib::deserialize(this->_mName) >> sp;

		dlib::array2d<dlib::rgb_pixel> img;
		load_image(img, this->_fName);
		pyramid_up(img);
		
		std::vector<dlib::rectangle> dets = detector(img, 1);
		dlib::full_object_detection shape = sp(img, dets[0]);
		this->_featureRect = face_frontal::FaceRectangle(
			face_frontal::Point(dets[0].left(), dets[0].top()),
			face_frontal::Point(dets[0].right(), dets[0].bottom())
			);
		
		for (unsigned long i = 0; i < shape.num_parts(); ++i) {
			this->_feature2D.push_back(
				cv::Point2f(shape.part(i).x(), shape.part(i).y())
				);
			std::cout << "(" << shape.part(i).x() << ", " << shape.part(i).y() << ")" << std::endl;
		}
		//dlib::array<array2d<rgb_pixel> > face_chips;
		//extract_image_chips(img, get_face_chip_details(shapes), face_chips);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
}

cv::Mat face_frontal::FaceFrontal2D::GetProjectionMat() {
	std::vector<cv::Point3f> p3d;
	for (int i = 0; i < this->p3d.rows; i++) {
		p3d.push_back(cv::Point3f(this->p3d.at<float>(i, 0),
			this->p3d.at<float>(i, 1),
			this->p3d.at<float>(i, 2)
			));
	}

	cv::Mat distCoeffs = cv::Mat::zeros(5, 1, CV_32F);
	cv::Mat rvec, tvec;
	cv::solvePnP(p3d, this->_feature2D, this->outA, distCoeffs, rvec, tvec);
	
	cv::Mat R;
	cv::Rodrigues(rvec, R);
	cv::Mat ins;
	cv::hconcat(R, tvec, ins);
	ins.convertTo(ins, 5);
	cv::Mat proj = this->outA * ins;
	//std::cout << this->outA * ins << std::endl;
	//std::cout << this->outA.mul(ins) << std::endl;
	return proj;
}

cv::Mat face_frontal::FaceFrontal2D::frontalization() {
	//generate p2d
	FaceFrontalFeature2D();

	//resize
	cv::Mat crop = this->_originImg(cv::Rect(this->_featureRect.left(), this->_featureRect.top(),
		this->_featureRect.width(), this->_featureRect.height()));

	cv::Mat resize_img = cv::Mat();
	cv::resize(crop, resize_img, cv::Size(WIDTH, HEIGHT));

	//concat
	cv::Mat ref3dface;
	cv::hconcat(this->refU, cv::Mat::ones(cv::Size(1, this->refU.rows), CV_32F), ref3dface);
	ref3dface = ref3dface.t();

	//get projection matrix
	cv::Mat projection_matrix = GetProjectionMat();
	cv::Mat proj3d = projection_matrix * ref3dface;
	cv::Mat_<double> proj2dtmp = cv::Mat_<double>(proj3d.rows-1, proj3d.cols, CV_64F);
	for (int i = 0; i < proj3d.cols; i++) {
		proj2dtmp.at<double>(0, i) = proj3d.at<float>(0, i) / proj3d.at<float>(2, i);
		proj2dtmp.at<double>(1, i) = proj3d.at<float>(1, i) / proj3d.at<float>(2, i);
	}

	cv::Mat_<bool> bgids = cv::Mat_<bool>(1, this->refU.rows);
	for (int i = 0; i < this->refU.rows; i++) {
		bgids.at<bool>(0, i) = (this->refU.at<float>(i, 1) < 0) ? true : false;
	}

	//proj dot, vlids logical
	cv::Mat_<bool> vlids = cv::Mat_<bool>(1, this->refU.rows);
	int cnt = 0;
	for (int i = 0; i < proj2dtmp.cols; i++) {
		double tmp1 = proj2dtmp.at<double>(0, i);
		double tmp2 = proj2dtmp.at<double>(1, i);

		vlids.at<bool>(0, i) =
			(((tmp1 > 0) & (tmp2 > 0)) &
			((tmp1 < resize_img.cols - 1) & (tmp2 < resize_img.rows - 1))) & bgids.at<bool>(0, i);
		
		if (vlids.at<bool>(0, i))
			cnt++;
	}
	
	cv::Mat_<double> proj2d_valid = cv::Mat_<double>(2, cnt);
	int valid_cnt = 0;
	for (int i = 0; i < vlids.cols; i++) {
		if (vlids.at<bool>(0, i)) {
			if (proj2dtmp.at<double>(0, i) < 0)
				std::cout << proj2dtmp.at<double>(0, i) << std::endl;
			if (proj2dtmp.at<double>(1, i) < 0)
				std::cout << proj2dtmp.at<double>(1, i) << std::endl;

			proj2d_valid.at<double>(0, valid_cnt) = proj2dtmp.at<double>(0, i);
			proj2d_valid.at<double>(1, valid_cnt) = proj2dtmp.at<double>(1, i);
			valid_cnt++;
		}
	}

	std::vector<int> indexFrontal = std::vector<int>();
	for (int i = 0; i < vlids.cols; i++) {
		if (vlids.at<bool>(0, i)) {
			indexFrontal.push_back(i);
		}
	}
	cv::Mat raw_frontal = cv::Mat(320, 320, CV_64FC3);
	cv::Mat_<double> te = proj2d_valid.t();
	bilinearInterp(resize_img, te, indexFrontal, cv::Size(320, 320), raw_frontal);
	cv::imshow("ejejejej", raw_frontal);
	cv::waitKey(0);
	//cv::Mat ch1, ch2, ch3;
	//std::vector<cv::Mat> raw_frontal_channels(3);
	//std::vector<cv::Mat> resize_img_channels(3);
	//// get the channels (follow BGR order in OpenCV)
	//cv::split(resize_img, resize_img_channels);
	//
	//for (int i = 0; i < raw_frontal_channels.size(); i++) {
	//	resize_img_channels[i];
	//	raw_frontal_channels[i];
	//}
	//// modify channel then merge
	//merge(raw_frontal_channels, raw_frontal);

	////ravel multi index
	//cv::Mat inds = cv::Mat(1, proj2d_valid.cols, CV_32F);
	//for (int i = 0; i < proj2d_valid.cols; i++) {
	//	int n1 = (int)round(proj2d_valid.at<double>(0, i));
	//	int n2 = (int)round(proj2d_valid.at<double>(1, i));

	//	inds.at<int>(0, i) = n2 * face_frontal::WIDTH + n1;
	//}

	//const int* p = inds.ptr<int>(0);
	//std::vector<int> inds_vect(p, p + inds.cols);
	//std::vector<int> inds_cnt = inds_vect;

	//std::set<int> inds_tmp(inds_vect.begin(), inds_vect.end());
	//inds_cnt.assign(inds_tmp.begin(), inds_tmp.end());

	////각 요소에 몇번씩 중복되었는지
	////http://stackoverflow.com/questions/1204313/counting-occurences-in-a-vector
	//std::sort(inds_vect.begin(), inds_vect.end());
	//std::vector<int> conts = std::vector<int>();

	////cv::Mat synth_front = cv::Mat::zeros(cv::Size(320, 320), CV_32F);

	return cv::Mat();
}

void face_frontal::FaceFrontal2D::bilinearInterp(cv::Mat& src, cv::Mat_<double>& pos, std::vector<int>& indexFrontal, cv::Size dstSize, cv::Mat& dst)
{
	using namespace cv;

	int dstHeight = dstSize.height, dstWidth = dstSize.width;

	//CV_Assert(pos.cols == 2 && static_cast<int>(pos.rows) == static_cast<int>(indexFrontal.size()));

	dst.create(dstHeight, dstWidth, src.type());

	const float* posPtr;
	float dx, dy;
	int x, y, row, col;
	int nChannel = src.channels();
	int sizeIndexFrontal = indexFrontal.size();

	if (nChannel == 1){
		for (int i = 0; i < sizeIndexFrontal; ++i){
			posPtr = pos.ptr<float>(i);

			x = static_cast<int>(posPtr[1]);
			y = static_cast<int>(posPtr[0]);
			dx = posPtr[1] - x;
			dy = posPtr[0] - y;

			row = indexFrontal[i] % dstHeight;
			col = indexFrontal[i] / dstHeight;

			dst.at<uchar>(row, col) = saturate_cast<uchar>((1 - dx)*(1 - dy)*src.at<uchar>(x, y) +
				(1 - dx)*dy*src.at<uchar>(x, y + 1) +
				dx*(1 - dy)*src.at<uchar>(x + 1, y) +
				dx*dy*src.at<uchar>(x + 1, y + 1));
		}
	}
	else if (nChannel == 3){
		Vec3b tmp1, tmp2, tmp3, tmp4;
		for (int i = 0; i < sizeIndexFrontal; ++i){
			posPtr = pos.ptr<float>(i);

			x = static_cast<int>(pos[i][1]); // here must be careful
			y = static_cast<int>(pos[i][0]);
			dx = posPtr[1] - x;
			dy = posPtr[0] - y;

			tmp1 = src.at<Vec3b>(x, y);
			tmp2 = src.at<Vec3b>(x, y + 1);
			tmp3 = src.at<Vec3b>(x + 1, y);
			tmp4 = src.at<Vec3b>(x + 1, y + 1);

			row = indexFrontal[i] % dstHeight;
			col = indexFrontal[i] / dstHeight;

			dst.at<Vec3b>(row, col)[0] = saturate_cast<uchar>((1 - dx)*(1 - dy)*tmp1[0] +
				(1 - dx)*dy*tmp2[0] + dx*(1 - dy)*tmp3[0] + dx*dy*tmp4[0]);
			dst.at<Vec3b>(row, col)[1] = saturate_cast<uchar>((1 - dx)*(1 - dy)*tmp1[1] +
				(1 - dx)*dy*tmp2[1] + dx*(1 - dy)*tmp3[1] + dx*dy*tmp4[1]);
			dst.at<Vec3b>(row, col)[2] = saturate_cast<uchar>((1 - dx)*(1 - dy)*tmp1[2] +
				(1 - dx)*dy*tmp2[2] + dx*(1 - dy)*tmp3[2] + dx*dy*tmp4[2]);
			std::cout << i << " // " << "(" << row << ", " << col << ")" << std::endl;
		}
	}
	else{
		return;
	}
}
//void face_frontal::FaceFrontal2D::rescale() {
//	cv::Mat reslace_tmp = this->_originImg(
//		cv::Rect(this->_featureRect.left,
//		this->_featureRect.top,
//		this->_featureRect.width,
//		this->_featureRect.height));
//
//	cv::resize(reslace_tmp, this->_rescaleImg, cv::Size(face_frontal::WIDTH, face_frontal::HEIGHT));
//}